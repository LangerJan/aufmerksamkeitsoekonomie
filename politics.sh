#!/usr/bin/env bash

WORKINGFOLDER=$(date +%s)

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd $SCRIPTDIR
pushd .

mkdir -p $WORKINGFOLDER
cd $WORKINGFOLDER

while read in; do
	DOMAIN=$(echo "$in" |  sed -e 's|^[^/]*//||' -e 's|/.*$||')

	echo "$in : $DOMAIN"
	w3m -dump -4 -T text/html $in > $DOMAIN.raw 2> /dev/null

	echo -n "#\"date\"" >> $DOMAIN.partei
	while read r; do
		echo -n "\"$r\";" >> $DOMAIN.partei
	done < ../parteien.txt


	echo "" >> $DOMAIN.partei
	echo -n `date +"%Y-%m-%d-%H:%M:%S;"` >> $DOMAIN.partei

	while read r; do
		count=$(grep -ic "$r" $DOMAIN.raw)
		echo -n "$count;" >> $DOMAIN.partei
	done < ../parteien.txt
	echo "" >> $DOMAIN.partei


	echo -n "#\"date\"" >> $DOMAIN.politiker
	while read r; do
		echo -n "\"$r\";" >> $DOMAIN.politiker
	done < ../politiker.txt

	echo "" >> $DOMAIN.politiker
	echo -n `date +"%Y-%m-%d-%H:%M:%S;"` >> $DOMAIN.politiker

	while read r; do
		count=$(grep -ic "$r" $DOMAIN.raw)
		echo -n "$count;" >> $DOMAIN.politiker
	done < ../politiker.txt
	echo "" >> $DOMAIN.politiker

done < ../medien.txt

popd
