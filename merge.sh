#!/usr/bin/env bash

SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd .

cd $SCRIPTDIR

MERGEDIR='merge/'

rm -rf $MERGEDIR
mkdir -p $MERGEDIR


for D in */; do
	if [[ "$D" != "$MERGEDIR" ]]; then
		echo $D
		echo $MERGEDIR
		while read in; do
			DOMAIN=$(echo "$in" |  sed -e 's|^[^/]*//||' -e 's|/.*$||')
			
			if [ ! -f $MERGEDIR$DOMAIN.partei ]; then
				head --lines=1 $D$DOMAIN.partei >> $MERGEDIR$DOMAIN.partei
			fi

			if [ ! -f $MERGEDIR$DOMAIN.politiker ]; then
				head --lines=1 $D$DOMAIN.politiker >> $MERGEDIR$DOMAIN.politiker
			fi

			tail --lines=1 $D$DOMAIN.partei >> $MERGEDIR$DOMAIN.partei
			tail --lines=1 $D$DOMAIN.politiker >> $MERGEDIR$DOMAIN.politiker
		done < medien.txt
	fi
done

for f in $MERGEDIR*.partei
do
	echo "$f"
	gnuplot -e "infile='$f'" -e "outfile='$f.png'" -e "titel='Partei-Erwähnungen auf $f'" -c ./plotscript
done


for f in $MERGEDIR*.politiker
do
	echo "$f"
	gnuplot -e "infile='$f'" -e "outfile='$f.png'" -e "titel='Politiker-Erwähnungen auf $f'" -c ./plotscript_politiker
done

popd
